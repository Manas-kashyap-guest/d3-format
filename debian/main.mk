#!/usr/bin/make -f
# Replacement for upstream's rollup-based buildsystem

SOURCES = \
	formatDecimal \
	exponent \
	formatGroup \
	formatDefault \
	formatPrefixAuto \
	formatRounded \
	formatTypes \
	formatSpecifier \
	locale \
	defaultLocale \
	precisionFixed \
	precisionPrefix \
	precisionRound

EXPORT_VAR = \
	-e '/^export var $(1);/d' \
	-e 's/^\( *\)\($(1) = .*\)/\1exports.\2/g'

EXPORT_PROTOTYPE = \
	-e '/function $(1)/,/^}/{s	^}	}\n\n$(1).prototype = $(2).prototype; // instanceof	g}'

HEADER = \
	python -c 'import json; x = json.load(open("package.json")); \
	  print("// %s Version %s Copyright %s" % (x["homepage"], x["version"], x["author"]["name"]))'

ALL = build/d3-format.js build/d3-format.min.js

all: $(ALL)

build/%.js: debian/rules debian/%.js.header debian/%.js.footer
	mkdir -p build && rm -f "$@" && touch "$@"
	$(HEADER) >> "$@"
	cat debian/$*.js.header >> "$@"
	set -e; for i in $(SOURCES); do \
		sed \
		  $(call EXPORT_VAR,format) \
		  $(call EXPORT_VAR,formatPrefix) \
		  -e 's/export default function.*(/function '"$$i"'(/g' \
		  -e '/^import .* from/d' \
		  -e 's/^export var/var/g' \
		  -e 's/export default {/var '"$$i"' = {/g' \
		  -e 's/function locale/function formatLocale/' \
		  $(call EXPORT_PROTOTYPE,formatSpecifier,FormatSpecifier) \
		  "src/$$i.js" >> "$@"; \
	done
	cat debian/$*.js.footer >> "$@"

build/%.min.js: build/%.js
	uglifyjs --preamble "$$($(HEADER))" "$<" -c -m -o "$@"

clean:
	rm -rf $(ALL)

.PHONY: all check clean
